import datetime
import random
import numpy as np
from numpy import genfromtxt
import os
import time
from collections import Counter
import re
import html2text
import matplotlib.pyplot as plt

hamCount = 70
spamCount = 40

#Converts a text to an int array with the corresponding positions in the frequency matrix
def textToIntArr(text):
    textArr = getTextArr(text)
    textInts = [token_start] + [wordPositions[w] if w in wordPositions and w != 'enron' else token_unknown for w in textArr[:inputLength - 2]] + [token_end]
    if len(textInts) < inputLength:
        textInts = [token_padding] * (inputLength - len(textInts)) + textInts
    return textInts

#Split text by words and special chars
def getTextArr(text):
    text = text.lower()
    text = text.replace(".", " . ")
    text = text.replace(",", " , ")
    text = text.replace("!", " ! ")
    text = text.replace("?", " ? ")


    if text.startswith("subject: "):
        text = text[len("subject: "):]

    text = re.sub('[^0-9a-zA-Z\s.,!?]+', ' ', text)

    #Replace digits with a token, so the model still differentiates between numbers of different length
    text = re.sub('\d', '[DIGIT]', text)
    return text.split()

#Converter for the emails from Oliver
def textFromOliverMail(text):
    text = text[text.find("Subject: "):]
    subject = text[:text.find("\n") + 1]

    text = text[text.find("Date: "):]
    text = text[text.find("\n"):]

    text = subject + text

    try:
        h = html2text.HTML2Text()
        h.ignore_links = True
        text = h.handle(text)
    except:
        text = None


    return text

dirHam = os.listdir('D:\Enron Emails\enron\ham')
dirSpam = os.listdir('D:\Enron Emails\enron\spam')

def processMail(text):
    text = text[len("Subject: "):]
    text = text.replace("\n", ".", 1)

    text = re.sub('[^0-9a-zA-Z\s.,!?]+', ' ', text)
    text = re.sub(',', ' , ', text)
    text = re.sub('\r', ' ', text)
    text = re.sub('\n', ' ', text)

    text = ' '.join(text.split())

    textArr = re.split('([.!?])', text)
    if textArr[-1] != '':
        textArr += [' .']

    textArr = [(textArr[i] + textArr[i + 1]).strip() for i in range(0, len(textArr) - 1, 2)]
    return textArr


allSentencesHam = []
allSentencesSpam = []

for i in range(hamCount):
    file = dirHam[i]
    text = (open('D:\Enron Emails\enron\ham\\' + file, 'rb').read()).decode('utf-8', 'ignore')
    textArr = processMail(text)
    allSentencesHam += textArr

for i in range(spamCount):
    file = dirSpam[i]
    text = (open('D:\Enron Emails\enron\spam\\' + file, 'rb').read()).decode('utf-8', 'ignore')
    textArr = processMail(text)
    allSentencesSpam += textArr

allSentencesHamFilter = []
allSentencesSpamFilter = []

for s in allSentencesHam:
    if len(s.split()) > 4:
        allSentencesHamFilter += [s]

for s in allSentencesSpam:
    if len(s.split()) > 4:
        allSentencesSpamFilter += [s]


allSentencesNP = np.array([[i, allSentencesHamFilter[i], 0] for i in range(len(allSentencesHamFilter))] + [[i + len(allSentencesHamFilter), allSentencesSpamFilter[i], 1] for i in range(len(allSentencesSpamFilter))])

np.savetxt("allSentences.csv", allSentencesNP, delimiter=";", fmt='%s')


splitArr = [[], [], [], []]
for s in allSentencesNP:
    randSample = random.sample(range(0, 4), 2)
    splitArr[randSample[0]].append(s)
    splitArr[randSample[1]].append(s)

np.savetxt("sentences1.csv", np.array(splitArr[0]), delimiter=";", fmt='%s')
np.savetxt("sentences2.csv", np.array(splitArr[1]), delimiter=";", fmt='%s')
np.savetxt("sentences3.csv", np.array(splitArr[2]), delimiter=";", fmt='%s')
np.savetxt("sentences4.csv", np.array(splitArr[3]), delimiter=";", fmt='%s')

print(allSentencesNP)
input()