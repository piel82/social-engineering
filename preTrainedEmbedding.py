import tensorflow as tf
from tensorflow import keras
import os
import re
from collections import Counter
import numpy as np
import datetime

is_windows = (os.name == 'nt')
configproto = tf.compat.v1.ConfigProto() 
configproto.gpu_options.allow_growth = True
sess = tf.compat.v1.Session(config=configproto) 
tf.compat.v1.keras.backend.set_session(sess)

# Decide wehter the model should be loaded or trained or both
loadModel = True
trainModel = False

# Model paths
loadModelPath = "checkpoints" + ("\\" if is_windows else "/") + "model.06-0.5998.h5"
saveModelPath = "models" + ("\\" if is_windows else "/") + "sentence_m1"


alpha = 0.001 # Learnrate
epochs = 10 # Epochs

path_to_glove_file = os.path.join("glove.6B.100d.txt")

# Only take the top 5000 words of the Embedding dict (to possibly reduce overfitting)
maxWords = 5000
c = 0
embeddings_index = {}
word_index = {}

# Read embeddings
with open(path_to_glove_file, encoding="utf8") as f:
    for line in f:
        word, coefs = line.split(maxsplit=1)
        word_index[word] = c
        coefs = np.fromstring(coefs, "f", sep=" ")
        embeddings_index[word] = coefs
        c += 1
        if c >= maxWords:
            break

print(embeddings_index)
input()
print("Found %s word vectors." % len(embeddings_index))

# Add tokens for padding and unknown words
tok_padding = len(embeddings_index)
tok_unknown = len(embeddings_index) + 1
num_tokens = len(embeddings_index) + 2
embedding_dim = 100

# Prepare embedding matrix
embedding_matrix = np.zeros((num_tokens, embedding_dim))
for word, i in word_index.items():
    embedding_vector = embeddings_index.get(word)
    if embedding_vector is not None:
        embedding_matrix[i] = embedding_vector

# Read the trainings data
text = (open('dataset_finished.csv', 'rb').read()).decode('utf-8', 'ignore')

lines = text.split("\n")

# Shuffle with fixed seeds so that the labels are distributed randomly
np.random.seed(0)
lines = lines[1:-1]
np.random.shuffle(lines)

inputLength = 50

dataX = np.empty((len(lines), inputLength))
dataY = np.empty((len(lines), 1))

# Fill the data arrays
for i in range(len(lines)):
    vals = lines[i].split(";")
    sentence = vals[1].split()
    sentence = sentence[:inputLength]
    sentence = [tok_padding] * (inputLength - len(sentence)) + [word_index[w] if w in word_index else tok_unknown for w in sentence]
    labels = [int(vals[k + 3]) for k in range(1)]

    dataX[i] = sentence
    dataY[i] = np.array(labels)



# Load or build the model
if loadModel:
    model = keras.models.load_model(loadModelPath)
else:
    embedding_layer = tf.keras.layers.Embedding(num_tokens,
                                                embedding_dim,
                                                embeddings_initializer=keras.initializers.Constant(embedding_matrix),
                                                trainable=False)  

    int_sequences_input = keras.Input(shape=(inputLength,), dtype="int64")
    embedded_sequences = embedding_layer(int_sequences_input)
    x = keras.layers.LSTM(80, return_sequences=True)(embedded_sequences)
    x = keras.layers.LSTM(80)(x)
    x = keras.layers.Dense(128, activation="relu")(x)
    x = keras.layers.Dense(20, activation="relu")(x)
    preds = keras.layers.Dense(1, activation="sigmoid")(x)

    model = keras.Model(int_sequences_input, preds)
    model.summary()

    model.compile(optimizer=tf.keras.optimizers.Adam(learning_rate=alpha),
                  loss='binary_crossentropy', 
                  metrics=['accuracy', tf.keras.metrics.FalsePositives(),  tf.keras.metrics.FalseNegatives()])

# Train the model
if trainModel:
    log_dir = "logs" + ("\\" if is_windows else "/") + "fit" + ("\\" if is_windows else "/") + datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
    tensorboard_callback = tf.keras.callbacks.TensorBoard(log_dir=log_dir, histogram_freq=1)

    checkpoint_path = "checkpoints" + ("\\" if is_windows else "/")# + datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
    cp_callback = tf.keras.callbacks.ModelCheckpoint(
                    filepath=checkpoint_path + 'model.{epoch:02d}-{val_loss:.4f}.h5', 
                    verbose=1, 
                    save_weights_only=False,
                    save_freq='epoch')

    # Fit
    model.fit(x=dataX, 
              y=dataY,
              shuffle=True,
              epochs=epochs,
              batch_size=32,
              validation_split=0.1,
              verbose=2,
              callbacks=[tensorboard_callback, cp_callback])

    model.save(saveModelPath)


# Use the data that has been used as validation data in the training as test data (due to small dataset size)
valDataX = dataX[int(-len(dataX)*0.1):]
valDataY = dataY[int(-len(dataY)*0.1):]
print(len(valDataX))
input()

model.evaluate(valDataX, 
               valDataY)