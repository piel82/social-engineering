import tensorflow as tf
from tensorflow import keras
import os
import re
from collections import Counter
import numpy as np
import datetime



is_windows = (os.name == 'nt')
configproto = tf.compat.v1.ConfigProto() 
configproto.gpu_options.allow_growth = True
sess = tf.compat.v1.Session(config=configproto) 
tf.compat.v1.keras.backend.set_session(sess)

# Model path
loadModelPath = "models" + ("\\" if is_windows else "/") + "sentence_m1"

#
alpha = 0.001 # Learnrate
epochs = 10 # Epochs

# Load glove dataset
path_to_glove_file = os.path.join("glove.6B.100d.txt")

# Only take the top 5000 words of the Embedding dict (to possibly reduce overfitting)
maxWords = 5000
c = 0
embeddings_index = {}
word_index = {}

# Read embeddings
with open(path_to_glove_file, encoding="utf8") as f:
    for line in f:
        word, coefs = line.split(maxsplit=1)
        word_index[word] = c
        coefs = np.fromstring(coefs, "f", sep=" ")
        embeddings_index[word] = coefs
        c += 1
        if c >= maxWords:
            break

print(embeddings_index)
input()
print("Found %s word vectors." % len(embeddings_index))

tok_padding = len(embeddings_index)
tok_unknown = len(embeddings_index) + 1
num_tokens = len(embeddings_index) + 2
embedding_dim = 100
hits = 0
misses = 0

# Function to prepare text into sentences
def processText(text):
    text = text.lower()
    text = re.sub('[^0-9a-zA-Z\s.,!?]+', ' ', text)
    text = re.sub('\.', ' . ', text)
    text = re.sub(',', ' , ', text)
    text = re.sub('!', ' ! ', text)
    text = re.sub('\?', ' ? ', text)
    text = re.sub('\r', ' ', text)
    text = re.sub('\n', ' ', text)

    text = ' '.join(text.split())

    textArr = re.split('([.!?])', text)
    #if textArr[-1] != '' or textArr[-1] != ' ':
    #    textArr += [' .']

    textArr = [(textArr[i] + textArr[i + 1]).strip() for i in range(0, len(textArr) - 1, 2)]
    return textArr

# Prepare embedding matrix
embedding_matrix = np.zeros((num_tokens, embedding_dim))
for word, i in word_index.items():
    embedding_vector = embeddings_index.get(word)
    if embedding_vector is not None:
        embedding_matrix[i] = embedding_vector
        hits += 1
    else:
        misses += 1

inputLength = 50

# Load model
model = keras.models.load_model(loadModelPath)

text = open('predict_text.txt', 'rb').read().decode('utf-8', 'ignore')

textArr = processText(text)

dataX = np.empty((len(textArr), inputLength))

# Read sentences from file 'predict_text.txt' and put them into the dataX array
for i in range(len(textArr)):
    sentence = textArr[i].split()
    sentence = sentence[:inputLength]
    sentence = [tok_padding] * (inputLength - len(sentence)) + [word_index[w] if w in word_index else tok_unknown for w in sentence]

    dataX[i] = sentence

result = model.predict(dataX)

# Print results
for i in range(len(result)):
    print(textArr[i])
    print(result[i])
    print("\n\n")
    input()