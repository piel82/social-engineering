import pandas as pd
import csv

# read csv Files
set_1 = pd.read_csv("sentences1.csv", sep=';', index_col=0)
set_2 = pd.read_csv("sentences2.csv", sep=';', index_col=0)
set_3 = pd.read_csv("sentences3.csv", sep=';', index_col=0)
set_4 = pd.read_csv("sentences4.csv", sep=';', index_col=0)

df1 = set_1[:3]
df2 = set_2[:3]
df3 = set_3[:3]
df4 = set_4[:3]


# delete duplicates and sort
full_df = pd.concat([df1,df2,df3,df4])
unique_df = full_df.drop_duplicates(keep='first')
sorted_df = unique_df.sort_index()  # not really needed

# create output DataFrames
check_output = pd.DataFrame([],columns=['sentence','label'])
correct_output = pd.DataFrame([],columns=['sentence','label'])

# write in each output
n = sorted_df.index.tolist()[-1]
i = 0
while i <= n:
    if sorted_df.loc[i,:].size >= 4:        # sentence is double
        check_output = check_output.append(sorted_df.loc[i,:])
    else:
        correct_output = correct_output.append(sorted_df.loc[i, :])
    i += 1

# writing csv Files
check_output.to_csv("check_labels.csv",sep=';')
correct_output.to_csv("correct_labels.csv",sep=';')